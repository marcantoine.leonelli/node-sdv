const express = require("express");
const dotenv = require("dotenv");
const app = express();
const authenticate = require("./src/middlewares/authentication");

dotenv.config();
const port = process.env.PORT || 4000;

const db = require("./db");
db.connect();

app.use(express.json());

const routes = [
  { path: "/blog", router: require("./src/blog/router")},
  { path: "/account", router: require("./src/account/router"), secure: true },
  { path: "/profile", router: require("./src/profile/router"), secure: true },
];

routes.forEach((route) => {
  if (route.secure) {
    app.use(route.path, authenticate, route.router);
  } else {
    app.use(route.path, route.router);
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
