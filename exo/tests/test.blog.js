//tests scripts for blog router with jest
const request = require("supertest");
const app = require("../app");

describe("GET /blog", function () {
  it("responds with json", function (done) {
    request(app).get("/blog").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
  });
});

describe("GET /blog/:id", function () {
  it("responds with json", function (done) {
    request(app).get("/blog/1").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
  });
});
