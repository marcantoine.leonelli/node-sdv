const ResponseMessages = {
  POST_NOT_FOUND: "Post not found",
  POST_CONTENT_REQUIRED: "Content is required",
  POST_TITLE_REQUIRED: "Title is required",
  INVALID_POST_BODY_LENGTH: (min, max) => `Content must be between ${min} and ${max} characters`,
  INVALID_PASSWORD_OR_EMAIL: "Invalid password or email",
  EMAIL_AND_PASSWORD_REQUIRED: "Email and password are required",
  INVALID_TOKEN: "Invalid token",
};

module.exports = { ResponseMessages };
