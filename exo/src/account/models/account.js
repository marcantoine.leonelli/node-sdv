const mongoose = require("mongoose");
const jwt  = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { uuid } = require("uuid");
const { passwordValidator, emailValidator } = require("../validator");

const accountSchema = new mongoose.Schema({
  id: {
    type: String,
    default: uuid,
    index: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    validator: passwordValidator,
  },
  email: {
    type: String,
    required: true,
    validator: emailValidator,
  },
});

accountSchema.pre("save", function (next) {
  const account = this;

  if (!account.isModified("password")) {
    return next();
  }

  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return next(err);
    }

    bcrypt.hash(account.password, salt, (err, hash) => {
      if (err) {
        return next(err);
      }

      account.password = hash;
      next();
    });
  });
});

accountSchema.methods.comparePassword = function (candidatePassword) {
  const account = this;

  return new Promise((resolve, reject) => {
    bcrypt.compare(candidatePassword, account.password, (err, isMatch) => {
      if (err) {
        return reject(err);
      }

      resolve(isMatch);
    });
  });
}

accountSchema.methods.generateToken = function () {
  const account = this;

  const today = new Date();
  const exp = new Date(today);
  exp.setDate(today.getDate() + 7);

  return jwt.sign({
    id: account.id,
    email: account.email,
    exp: parseInt(exp.getTime() / 1000)
  }, process.env.JWT_SECRET);
}



module.exports = Account = mongoose.model("Account", accountSchema);
