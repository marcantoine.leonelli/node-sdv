const router = require("express").Router();
const Account = require("./models/account");
const jwt  = require("jsonwebtoken");
const { ResponseMessages } = require("../responseMessages");

const emailAndPassWordRequiredMiddleWare = (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    res.status(400).json(ResponseMessages.EMAIL_AND_PASSWORD_REQUIRED);
    return;
  }

  next();
};

router.post("/login", emailAndPassWordRequiredMiddleWare, async (req, res) => {
  const { email, password } = req.body;

  try {
    let account = await Account.findOne({ email });

    await new Promise((resolve) => setTimeout(resolve, 1000));

    if (!account) {
      res.status(404).json(ResponseMessages.INVALID_PASSWORD_OR_EMAIL);
      return;
    }

    if (await account.comparePassword(password)) {
      return res.status(200).send({ id: account.id, email: account.email, token: await account.generateToken() });
    }

    res.status(404).json(ResponseMessages.INVALID_PASSWORD_OR_EMAIL);
  } catch (error) {
    console.error(error);
    res.status(500).send(error.message);
  }
});

router.post("/register", emailAndPassWordRequiredMiddleWare, async (req, res) => {
  const { email, password } = req.body;

  const account = new Account({
    email,
    password,
  });

  try {
    await account.save();

    res.status(201).json(account);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

router.get("/verify", async (req, res) => {
  const type = req.headers.authorization?.split(" ")[0];
  const token = req.headers.authorization?.split(" ")[1];

  if (type !== "Bearer") {
    res.status(400).json({error: ResponseMessages.INVALID_TOKEN});
    return;
  }

  const decoded = jwt.verify(token, process.env.JWT_SECRET);
  console.log(decoded);
  try {
    const account = await Account.findById(decoded.id);

    if (!account) {
      res.status(404).json({error: ResponseMessages.INVALID_TOKEN});
      return;
    }

    res.status(200).json({email: account.email, token});
  } catch (error) {
    res.status(500).json(err.message);
  }
});

module.exports = router;
