const passwordValidator = (val) => {
  const pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
  return pattern.test(val);
};

const emailValidator = (val) => {
  const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(val);
};

const nameValidator = (val) => {
  const pattern = /^[a-zA-Z]+$/;
  return pattern.test(val);
};

module.exports = {
  passwordValidator,
  emailValidator,
  nameValidator,
};
