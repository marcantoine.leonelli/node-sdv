const Person = require("../models/person");
const Company = require("../models/company");
const Profile = require("../models/profile");

const createProfile = async (req, res) => {
  const { kind, ...rest } = req.body;

  let Model;

  switch (kind) {
    case "Person":
      Model = Person;
      break;
    case "Company":
      Model = Company;
      break;
    default:
      res.status(400).json({ error: "Invalid kind" });
      return;
  }

  const profile = new Model(rest);

  try {
    await profile.save();
    res.status(201).json(profile);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const getProfile = async (req, res) => {
  const { id } = req.params;

  try {
    const profile = await Profile.find({ id: id }).lean().exec();

    res.status(200).json(profile);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const updateProfile = async (req, res) => {
  try {
    const profile = await Profile.findOneAndUpdate({ id: req.params.id }).lean().exec();

    res.status(200).json(profile);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

module.exports = {
  createProfile,
  getProfile,
  updateProfile,
};
