const mongoose = require("mongoose");
const Profile = require("./profile");

const companySchema = new mongoose.Schema(
  {
    companyName: {
      type: String,
    },
  },
  {
    discriminatorKey: "kind",
  }
);

module.exports = Company = Profile.discriminator("Company", companySchema);
