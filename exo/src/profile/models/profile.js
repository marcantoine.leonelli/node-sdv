const mongoose = require("mongoose");
const { uuid } = require("uuid");

const profileSchema = new mongoose.Schema({
  avatar: {
    type: String,
  },
  bio: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  email: {
    type: String,
  },
  id: {
    type: String,
    index: true,
    unique: true,
    default: uuid,
  },
  username: {
    type: String,
    required: true,
  },
});

module.exports = Profile = mongoose.model("Profile", profileSchema);
