const router = require("express").Router();
const Profile = require("./models/profile");
const { createProfile, getProfile, updateProfile } = require("./controllers/profileController");

const profileExistsMiddleware = (req, res, next) => {
  try {
    const profile = Profile.find({ id: req.params.id }).lean().exec();

    if (!profile) {
      res.status(404).json({ error: "Profile not found" });
      return;
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }

  next();
};

router.post("/", createProfile);

router.get("/:id", profileExistsMiddleware, getProfile);

router.put("/:id", profileExistsMiddleware, updateProfile);

module.exports = router;
