const { lenghtValidator } = require("./validator");
const Post = require("./models/post");
const { getAllPosts, getPostById, createPost, updatePost, removePost } = require("./controllers/postController");
const { getCommentsByPostId, createComment, updateComment, removeComment } = require("./controllers/commentController");
const { ResponseMessages } = require("../responseMessages");

const router = require("express").Router();

router.use((req, res, next) => {
  delete req.body.id;

  next();
});

const postExistsMiddeleware = async (req, res, next) => {
  const post = await Post.findOne({ id: req.params.id });

  if (!post) {
    return res.status(404).send(ResponseMessages.POST_NOT_FOUND);
  }

  req.post = post;

  next();
};

const contentBodyMiddleware = (req, res, next) => {
  const content = req.body.content;
  const title = req.body.title;

  if (!content) {
    return res.status(400).send(ResponseMessages.POST_CONTENT_REQUIRED);
  }
  if (!title) {
    return res.status(400).send(ResponseMessages.POST_TITLE_REQUIRED);
  }
  if (!lenghtValidator(content)(10, 200)) {
    return res.status(400).send(ResponseMessages.INVALID_POST_BODY_LENGTH(10, 200));
  }
  if (!lenghtValidator(title)(3, 50)) {
    return res.status(400).send(ResponseMessages.INVALID_POST_BODY_LENGTH(3, 50));
  }

  next();
};

const commentBodyMiddleware = (req, res, next) => {
  const content = req.body.content;

  if (!content) {
    return res.status(400).send(ResponseMessages.POST_CONTENT_REQUIRED);
  }
  if (!lenghtValidator(content)(10, 200)) {
    return res.status(400).send(ResponseMessages.INVALID_POST_BODY_LENGTH(10, 200));
  }

  next();
};

// get all posts
router.get("/posts", getAllPosts);

// get post by id
router.get("/posts/:id", postExistsMiddeleware, getPostById);

// create new post
router.post("/posts", contentBodyMiddleware, createPost);

// update post
router.patch("/posts/:id", postExistsMiddeleware, updatePost);

// delete post
router.delete("/posts/:id", postExistsMiddeleware, removePost);

// get all comments by post id
router.get("/posts/:id/comments", postExistsMiddeleware, getCommentsByPostId);

// create new comment
router.post("/posts/:id/comments", postExistsMiddeleware, commentBodyMiddleware, createComment);

// update comment
router.patch("/comments/:id", postExistsMiddeleware, updateComment);

// delete comment
router.delete("/comments/:id", postExistsMiddeleware, removeComment);

module.exports = router;
