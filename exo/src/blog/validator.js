const lenghtValidator = (value) => (minLength, maxLength) => {
  return value.length <= maxLength && value.length >= minLength;
};

module.exports = {
  lenghtValidator,
};
