const Comment = require("../models/comment");
const { uuid } = require("uuid");

const getCommentsByPostId = async (req, res) => {
  const comments = await Comment.find({ post: req.params.id }).lean().exec();

  res.status(200).json(comments);
};

const createComment = async (req, res) => {
  const comment = new Comment({
    ...req.body,
    post: req.post.id,
  });

  try {
    await comment.save();

    req.post.commentsCount++;
    console.log(req.post);
    await req.post.save();

    res.header("Location", "/blog/" + comment.id);
    res.status("201").json(comment);
  } catch (err) {
    res.status("500").send(err.message);
  }
};

const updateComment = async (req, res) => {
  const post = await Post.findOneAndUpdate({ id: req.params.id }, req.body, {
    new: true,
    runValidators: true,
  }).lean();

  res.status(200).json(post);
};

const removeComment = async (req, res) => {
  try {
    const comment = await Comment.deleteOne({ id: req.params.id }).lean().exec();

    req.post.commentsCount--;
    await req.post.save();

    res.status(200).json(comment);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  getCommentsByPostId,
  createComment,
  updateComment,
  removeComment,
};
