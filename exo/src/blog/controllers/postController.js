const Post = require("../models/post");
const Comment = require("../models/comment");

const getAllPosts = async (req, res) => {
  const { page = 1, limit = 10 } = req.query;

  try {
    const posts = await Post.find()
      .limit(limit)
      .skip((page - 1) * limit)
      .lean()
      .exec();

    res.status(200).json({
      posts,
      totalPages: Math.ceil(posts.length / limit),
      currentPage: parseInt(page),
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getPostById = async (req, res) => {
  try {
    const post = await Post.findOne({ id: req.params.id }).lean().exec();

    res.status(200).json(post);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const createPost = async (req, res) => {
  const post = new Post({
    ...req.body,
    updateAt: Date.now(),
  });

  try {
    await post.save();

    res.header("Location", "/blog/" + post.id);
    res.status("201").json(post);
  } catch (err) {
    res.status("400").send(err.message);
  }
};

const updatePost = async (req, res) => {
  try {
    const post = await Post.findOneAndUpdate(
      { id: req.params.id },
      {
        ...req.body,
        updateAt: Date.now(),
      },
      {
        new: true,
        runValidators: true,
      }
    ).lean();

    res.status(200).json(post);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const removePost = async (req, res) => {
  try {
    const post = await Post.findOneAndDelete({ id: req.params.id }).lean();

    await Comment.deleteMany({ postId: req.params.id }).lean().exec();

    res.status(200).json(post);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  getAllPosts,
  getPostById,
  createPost,
  updatePost,
  removePost,
};
