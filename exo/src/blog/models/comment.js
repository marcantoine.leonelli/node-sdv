const mongoose = require("mongoose");
const { uuid } = require("uuid");

const commentSchema = new mongoose.Schema({
  id: {
    type: String,
    index: true,
    unique: true,
    default: uuid,
  },
  content: {
    type: String,
    required: true,
    minlength: 10,
    maxlength: 200,
  },
  postId: {
    type: String,
    ref: "Post",
  },
  createAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = Comment = mongoose.model("Comment", commentSchema);
