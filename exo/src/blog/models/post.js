const mongoose = require("mongoose");
const { uuid } = require("uuid");

const postSchema = new mongoose.Schema({
  id: {
    type: String,
    index: true,
    unique: true,
    default: uuid,
  },
  title: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50,
  },
  content: {
    type: String,
    required: true,
    minlength: 10,
    maxlength: 200,
  },
  createAt: {
    type: Date,
    default: Date.now,
  },
  updateAt: {
    type: Date,
    default: Date.now,
  },
  commentsCount: {
    type: Number,
    default: 0,
  },
  owner: {
    type: String,
    required: true,
  }
});

module.exports = Post = mongoose.model("Post", postSchema);
