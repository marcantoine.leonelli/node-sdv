const jwt = require("jsonwebtoken");
const { ResponseMessages } = require("../responseMessages");

const authenticate = (req, res, next) => {
  const type = req.headers.authorization?.split(" ")[0];
  const token = req.headers.authorization?.split(" ")[1];

  if (type !== "Bearer" || !token) {
    res.status(401).json(ResponseMessages.INVALID_TOKEN);
    return;
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    req.account = decoded;

    next();
  } catch (err) {
    res.status(401).json(ResponseMessages.INVALID_TOKEN);
  }
}

module.exports = authenticate;